//
//  Exercise+CoreDataProperties.swift
//  StayActive
//
//  Created by iosdev on 4.5.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//
//

import Foundation
import CoreData


extension Exercise {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Exercise> {
        return NSFetchRequest<Exercise>(entityName: "Exercise")
    }

    @NSManaged public var dateDone: Date?
    @NSManaged public var desc: String?
    @NSManaged public var difficulty: String?
    @NSManaged public var guide: String?
    @NSManaged public var guideImg: String?
    @NSManaged public var lenghtRep: Int64
    @NSManaged public var lengthAsSec: Int64
    @NSManaged public var name: String?
    @NSManaged public var repsDone: Int64
    @NSManaged public var timeTaken: Int64
    @NSManaged public var userID: String?
    @NSManaged public var whoIsDoing: User?

}
