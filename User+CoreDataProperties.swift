//
//  User+CoreDataProperties.swift
//  StayActive
//
//  Created by iosdev on 4.5.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var age: Int64
    @NSManaged public var gender: String?
    @NSManaged public var height: Double
    @NSManaged public var name: String?
    @NSManaged public var profileImg: String?
    @NSManaged public var uid: String?
    @NSManaged public var weight: Double
    @NSManaged public var worksOn: Exercise?

}
