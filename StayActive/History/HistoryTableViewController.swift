//
//  HistoryTableViewController.swift
//  StayActive
//
//  Created by iosdev on 5.5.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit
import CoreData
import FirebaseAuth

class HistoryTableViewController: UITableViewController {
    
    var exercisesList = [Exercise]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getExercises()
        self.tableView.rowHeight = 50.0
    }
    
    @IBAction func clearButtonPressed(_ sender: UIBarButtonItem) {
        print("Clear button pressed")
        
        let alert = UIAlertController(title: "Are you sure you want to clear all exercises?", message: "This action cannot be undone", preferredStyle: .alert)
        
        let color = #colorLiteral(red: 0.05362936854, green: 0.6396086812, blue: 0, alpha: 1)
        
        alert.view.tintColor = color
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: {action in
            let managedObjectContext = AppDelegate.viewContext
            let exerciseRequest:NSFetchRequest<Exercise> = Exercise.fetchRequest()
            if let exercises = try? managedObjectContext.fetch(exerciseRequest){
                for e in exercises{
                    if(e.userID == Auth.auth().currentUser?.uid){
                        managedObjectContext.delete(e)
                    }
                }
            }
            self.exercisesList.removeAll()
            print("Exercise data cleared")
            self.tableView.reloadData()
        }))
        self.present(alert, animated: true)
    }
    func getExercises(){
        print("Searching for exercises")
        let managedObjectContext = AppDelegate.viewContext
        let exerciseRequest:NSFetchRequest<Exercise> = Exercise.fetchRequest()
        if let exercises = try? managedObjectContext.fetch(exerciseRequest){
            for e in exercises{
                if(e.userID == Auth.auth().currentUser?.uid){
                    self.exercisesList.append(e)
                    print("Found exercise \(e.desc ?? "No name")")
                }
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exercisesList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "HistoryTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? HistoryTableViewCell  else {
            fatalError("The dequeued cell is not an instance of HistoryTableViewCell.")
        }
        
        let e = exercisesList[indexPath.row]
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy HH:mm"
        let dateAsString = formatter.string(from: e.dateDone ?? Date())
        
        cell.nameLabel.text = e.desc
        cell.dateLabel.text = dateAsString
        cell.repsLabel.text = String(e.repsDone)
        cell.timeLabel.text = String(e.timeTaken)
        
        return cell
    }
}
