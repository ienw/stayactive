//
//  Goals_EntryViewController.swift
//  StayActive
//
//  Created by iosdev on 23.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit
import CoreData

class Goals_EntryViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    @IBOutlet weak var picker: UIPickerView!
    
    var update: (() -> Void)?
    var pickerData = ["running", "Dips", "Regular Push-ups", "Diamond Push-ups", "Wall-Push-ups","Sit-ups", "Jumping Jacks", "Leg Raises", "Plank", "Squats", "Jumping Squats", "Backward Lunge", "Leg Lift" ]
    var level = ["Beginner", "Regular", "Athlete"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.picker.delegate = self
        self.picker.dataSource = self
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveTask))

        // Do any additional setup after loading the view.
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
           return 2
            
       }
       
       // The number of rows of data
       func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            if (component == 0) {
                return pickerData.count
            } else {
                return level.count
            }
       }
       
       // The data to return fopr the row and component (column) that's being passed in
       func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
           
            if (component == 0) {
                return pickerData[row]
            } else {
                return level[row]
            }
       }
    
    
    
    @objc func saveTask() {
        
        if (pickerData.count != 0) {
            let goalString = UserDefaults().value(forKeyPath: "dailygoals") as? String ?? "";
            
            var splitGoals = goalString.components(separatedBy: ",")
            
            let selectedValue = pickerData[picker.selectedRow(inComponent: 0)]
            let selectedLevel = level[picker.selectedRow(inComponent: 1)]
            
            let selectedExercise = "\(selectedValue);\(selectedLevel)"

            if (!splitGoals.contains(selectedExercise)) {
                splitGoals.append(selectedExercise)
            }
            
            UserDefaults().set(splitGoals.joined(separator: ","), forKey: "dailygoals")
        }
        
        update?()
        navigationController?.popViewController(animated: true)
    }
}
