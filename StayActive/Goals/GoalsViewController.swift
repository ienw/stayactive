//
//  GoalsViewController.swift
//  StayActive
//
//  Created by iosdev on 21.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

class GoalsViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!

    var goals = [String]()
    var done = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Goals"
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let doneString = UserDefaults().value(forKeyPath: "dailygoalsdone") as? String ?? "";
        
        let splitDone = doneString.components(separatedBy: ",")
        
        for doneTask in splitDone {
            done.append(doneTask)
        }
        
        // Get all current saved goals
        updateGoals()
    }
    
    func updateGoals() {
        
        goals.removeAll()
        
        let goalString = UserDefaults().value(forKeyPath: "dailygoals") as? String ?? "";
        
        let splitGoals = goalString.split(separator: ",")
        
        for goal in splitGoals {
            goals.append(String(goal))
        }
        
        tableView.reloadData()
    }
    
    
    @IBAction func didTapAdd() {
        
        let vc = storyboard?.instantiateViewController(identifier: "GoalsEntry") as! Goals_EntryViewController
        
        vc.title = "New Goal"
        vc.update = {
            DispatchQueue.main.async {
                self.updateGoals()
            }
        }
        
        navigationController?.pushViewController(vc, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension GoalsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selection = goals[indexPath.row]
        print(done, selection)
        // when row is clicked toggle the done state
        if (done.contains(selection)) {
            done = done.filter { $0 != selection }
        } else {
            done.append(selection)
        }
        print(done)
        UserDefaults().set(done.joined(separator: ","), forKey: "dailygoalsdone")
        
        tableView.deselectRow(at: indexPath, animated: true)

        tableView.reloadData()
    }
}

extension GoalsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return goals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let item = goals[indexPath.row]
        let label = item.replacingOccurrences(of: ";", with: " - ")
        
        let doneString = UserDefaults().value(forKeyPath: "dailygoalsdone") as? String ?? "";
        let splitDone = doneString.components(separatedBy: ",")
        
        // for done task display with strike through effect
        if (splitDone.contains(item)) {
            let strikedLabel: NSMutableAttributedString =  NSMutableAttributedString(string: label)
            strikedLabel.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, strikedLabel.length))
            cell.textLabel?.attributedText = strikedLabel
        } else {
            // Set striketrhough as 0 length to undo it
            let notStrikedLabel: NSMutableAttributedString =  NSMutableAttributedString(string: label)
            notStrikedLabel.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, 0))
            cell.textLabel?.attributedText = notStrikedLabel
        }
        
        return cell
    }
 
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath){
        
        if editingStyle == UITableViewCell.EditingStyle.delete{
            
            let selection = goals[indexPath.row]
            
            self.goals.remove(at: indexPath.row)
            UserDefaults().set(self.goals.joined(separator: ","), forKey: "dailygoals")
            
            
            // remove from done if there
            if (done.contains(selection)) {
                done = done.filter { $0 != selection }
                UserDefaults().set(done.joined(separator: ","), forKey: "dailygoalsdone")
            }
            
            /* DO THIS WHEN EXERSICE IS SAVED

             let finishedGoal = EXERCISE_NAME ex. "Sit-up"
             
             let goalString = UserDefaults().value(forKeyPath: "dailygoals") as? String ?? "";
             var splitGoals = goalString.components(separatedBy: ",")
             if (splitGoals.contains(finishedGoal)) {
             
                // set as done
                 let doneString = UserDefaults().value(forKeyPath: "dailygoalsdone") as? String ?? "";
                 var splitDone = doneString.components(separatedBy: ",")
                 if (!splitDone.contains(finishedGoal)) {
                    splitDone.append(finishedGoal)
                    UserDefaults().set(splitDone.joined(separator: ","), forKey: "dailygoalsdone")
                }
             }
            */
            
            tableView.reloadData()
        }
    }
}


