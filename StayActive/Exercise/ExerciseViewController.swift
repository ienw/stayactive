//
//  ExerciseViewController.swift
//  StayActive
//
//  Created by iosdev on 21.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

class ExerciseViewController: UIViewController {
    
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var levelSlider: UISlider!

    var difficulty = "Regular"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        levelLabel.text = difficulty
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        if(sender.value >= 0 && sender.value <= 0.5){
            difficulty = "Beginner"
            levelLabel.text = difficulty
            levelSlider.value = 0
        }
        if(sender.value > 0.5 && sender.value <= 1.5){
            difficulty = "Regular"
            levelLabel.text = difficulty
            levelSlider.value = 1
        }
        if(sender.value > 1.5 && sender.value <= 2){
            difficulty = "Athlete"
            levelLabel.text = difficulty
            levelSlider.value = 2
        }
    }
    
    //Create proper Exercise-object and send it to next view
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier ==
            "toCardioWorkout"){
            print("Opening Cardio workout");
            let vc = segue.destination as? SingleExerciseViewController
            let exerciseToSend = ExerciseObject(name: "Cardio", description: "Running", guide: Guides().getGuide(exercise: "Cardio"), difficulty: difficulty, lenghtAsSec: ExerciseData().getLengthAsSecs(difficulty: difficulty, exercise: "Running"), lenghtAsReps: ExerciseData().getLengthAsReps(difficulty: difficulty, exercise: "Running"))
            vc!.exercise = exerciseToSend
        }
        if (segue.identifier ==
            "toCoreWorkout"){
            print("Opening Core workout");
            let vc = segue.destination as? SingleExerciseViewController
            let exerciseToSend = ExerciseObject(name: "Core", description: "Sit-ups", guide: Guides().getGuide(exercise: "Sit-ups"), difficulty: difficulty, lenghtAsSec: ExerciseData().getLengthAsSecs(difficulty: difficulty, exercise: "Sit-Ups"), lenghtAsReps: ExerciseData().getLengthAsReps(difficulty: difficulty, exercise: "Sit-Ups"))
            vc!.exercise = exerciseToSend
        }
        if (segue.identifier ==
            "toArmsWorkout"){
            print("Opening Arms workout");
            let vc = segue.destination as? SingleExerciseViewController
            let exerciseToSend = ExerciseObject(name: "Arms", description: "Dips", guide: Guides().getGuide(exercise: "Dips"), difficulty: difficulty, lenghtAsSec: ExerciseData().getLengthAsSecs(difficulty: difficulty, exercise: "Dips"), lenghtAsReps: ExerciseData().getLengthAsReps(difficulty: difficulty, exercise: "Dips"))
            vc!.exercise = exerciseToSend
        }
        if (segue.identifier ==
            "toLegsWorkout"){
            print("Opening Legs workout");
            let vc = segue.destination as? SingleExerciseViewController
            let exerciseToSend = ExerciseObject(name: "Legs", description: "Squats", guide: Guides().getGuide(exercise: "Squats"), difficulty: difficulty, lenghtAsSec: ExerciseData().getLengthAsSecs(difficulty: difficulty, exercise: "Squats"), lenghtAsReps: ExerciseData().getLengthAsReps(difficulty: difficulty, exercise: "Squats"))
            vc!.exercise = exerciseToSend
        }
    }
    @IBAction func unwind( unwindSegue: UIStoryboardSegue) {
        print("Going back")
    }
}
