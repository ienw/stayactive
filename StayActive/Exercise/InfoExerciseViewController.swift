//
//  InfoExerciseViewController.swift
//  StayActive
//
//  Created by iosdev on 22.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

class InfoExerciseViewController: UIViewController {
    
    var exerciseInfo = ""
    var exerciseHeader = ""

    @IBOutlet weak var infoField: UITextView!
    @IBOutlet weak var infoHeader: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        infoHeader.text = exerciseHeader
        infoField.text = exerciseInfo
    }

}
