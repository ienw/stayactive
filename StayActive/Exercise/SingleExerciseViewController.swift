//
//  SingleExerciseViewController.swift
//  
//
//  Created by iosdev on 22.4.2020.
//

import UIKit
import CoreData
import AVFoundation
import FirebaseAuth

class SingleExerciseViewController: UIViewController, UITextFieldDelegate {
    
    var exercise = ExerciseObject(name: "default", description: "default", guide: "default", difficulty: "default", lenghtAsSec: 0, lenghtAsReps: 0)
    var timer: Timer!
    var timeLeft = 0
    var timerSound: AVAudioPlayer?
    var dataReceivedFromSelector = ""
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var exerciseLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var repsToDoLabel: UILabel!
    @IBOutlet weak var timeLeftLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var repLabel: UILabel!
    @IBOutlet weak var repField: UITextField!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var difficultyLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        repField.delegate = self
        timeLeft = exercise.lenghtAsSec
        exerciseLabel.text = exercise.name
        descLabel.text = exercise.description
        timeLabel.text = "\(timeLeft) s"
        difficultyLabel.text = exercise.difficulty
        if(exercise.name != "Cardio"){
            repsToDoLabel.text = "Try to do at least \(exercise.lenghtAsReps) reps"
        }else{
            repsToDoLabel.isHidden = true
            changeButton.isHidden = true
            changeButton.isUserInteractionEnabled = false
        }
    }
    
    //Allow only numbers in text field where reps are asked
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil {
            return true
        } else {
            return false
        }
    }
    
    //Function to play short bell sound
    func playSound(){
        let path = Bundle.main.path(forResource: "timerBell", ofType:"wav")!
        let url = URL(fileURLWithPath: path)
        
        do {
            timerSound = try AVAudioPlayer(contentsOf: url)
            timerSound?.play()
        } catch {
            print("Error playing the sound")
        }
    }
    
    //Function to save current exercise to core data
    func saveExercise(){
        let managedObjectContext = AppDelegate.viewContext
        let savedExercise = Exercise(context: managedObjectContext)
        savedExercise.name = exercise.name
        savedExercise.desc = exercise.description
        savedExercise.guide = exercise.guide
        savedExercise.guideImg = exercise.image
        savedExercise.dateDone = exercise.dateDone
        savedExercise.difficulty = exercise.difficulty
        savedExercise.lengthAsSec = Int64(exercise.lenghtAsSec)
        savedExercise.timeTaken = Int64(exercise.timeTaken)
        savedExercise.repsDone = Int64(exercise.repsDone)
        savedExercise.userID = Auth.auth().currentUser?.uid
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        print("Exercise saved", exercise.description, exercise.difficulty)
        
        // mark exercise as done for goals
        let finishedGoal = "\(exercise.description);\(exercise.difficulty)"
        let goalString = UserDefaults().value(forKeyPath: "dailygoals") as? String ?? "";
        let splitGoals = goalString.components(separatedBy: ",")
        if (splitGoals.contains(finishedGoal)) {
           // set as done
            let doneString = UserDefaults().value(forKeyPath: "dailygoalsdone") as? String ?? "";
            var splitDone = doneString.components(separatedBy: ",")
            if (!splitDone.contains(finishedGoal)) {
               splitDone.append(finishedGoal)
               UserDefaults().set(splitDone.joined(separator: ","), forKey: "dailygoalsdone")
           }
        }
    }
    
    //Function to check what exercises are saved to core data
    func checkExercises(){
        let managedObjectContext = AppDelegate.viewContext
        let exerciseRequest:NSFetchRequest<Exercise> = Exercise.fetchRequest()
        if let exercises = try? managedObjectContext.fetch(exerciseRequest){
            for e in exercises{
                if(e.userID == Auth.auth().currentUser?.uid){
                    print("Exercise: \(e.name ?? "no name")")
                }
            }
        }
    }
    
    @IBAction func stopButtonPressed(_ sender: UIButton) {
        print("Stop button pressed")
        timer.invalidate()
        if(exercise.name != "Cardio"){
            repField.isHidden = false
            repField.isUserInteractionEnabled = true
        }else{
            repLabel.text = "Good job! You ran for \(exercise.lenghtAsSec - timeLeft) seconds."
        }
        repLabel.isHidden = false
        saveButton.isHidden = false
        saveButton.isUserInteractionEnabled = true
        stopButton.isEnabled = false
        stopButton.isUserInteractionEnabled = false
    }
    
    
    @IBAction func startButtonPressed(_ sender: UIButton) {
        print("Start button pressed")
        stopButton.isEnabled = true
        stopButton.isUserInteractionEnabled = true
        startButton.isEnabled = false
        startButton.isUserInteractionEnabled = false
        self.timeLeftLabel.isHidden = false
        
        timeLeft = exercise.lenghtAsSec
        exerciseLabel.text = exercise.name
        descLabel.text = exercise.description
        timeLabel.text = "\(timeLeft) s"
        
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { timer in
            
            self.timeLeft -= 1
            self.timeLabel.text = "\(self.timeLeft) s"
            
            if self.timeLeft <= 0 {
                timer.invalidate()
                self.playSound()
                if(self.exercise.name != "Cardio"){
                    
                    self.repField.isHidden = false
                    self.repField.isUserInteractionEnabled = true
                }
                else{
                    self.repLabel.text = "Good job! You ran for \(self.exercise.lenghtAsSec - self.timeLeft) seconds"
                }
                self.repLabel.isHidden = false
                self.saveButton.isHidden = false
                self.saveButton.isUserInteractionEnabled = true
                self.stopButton.isEnabled = false
                self.stopButton.isUserInteractionEnabled = false
            }
        })
        
    }
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        if(repField.text!.count > 0 || exercise.name == "Cardio"){
            exercise.repsDone = Int(repField.text ?? "0") ?? 0
            repField.text = ""
            exercise.timeTaken = exercise.lenghtAsSec - timeLeft
            exercise.dateDone = Date()
            self.timeLabel.text = "Exercise saved!"
            self.timeLeftLabel.isHidden = true
            self.repLabel.isHidden = true
            self.repField.isHidden = true
            self.repField.isUserInteractionEnabled = false
            self.saveButton.isHidden = true
            self.saveButton.isUserInteractionEnabled = false
            self.startButton.isEnabled = true
            self.startButton.isUserInteractionEnabled = true
        }
        saveExercise()
        //checkExercises()
        print("Did \(exercise.repsDone) reps in \(exercise.timeTaken) seconds")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier ==
            "showInfo"){
            print("Opening info");
            let vc = segue.destination as? InfoExerciseViewController
            vc?.exerciseInfo = exercise.guide
            vc?.exerciseHeader = exercise.description
        }
        if (segue.identifier ==
             "showSelector"){
             print("Opening exercise selector");
             let vc = segue.destination as? ExerciseSelectionViewController
            vc?.dataFromSingleExerciseView = self.exercise
         }
    }
    
    @IBAction func unwind( unwindSegue: UIStoryboardSegue) {
        print("Going back")
        if let sourceViewController = unwindSegue.source as?
            ExerciseSelectionViewController {
            print("Getting new data")
            exercise = sourceViewController.newExercise
            viewDidLoad()
            
        }
    }
}
