//
//  ExerciseSelectionViewController.swift
//  StayActive
//
//  Created by iosdev on 30.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit
import CoreData
import FirebaseAuth

class ExerciseSelectionViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var currentExercise = ""
    var listOfCoreExercises = ["Sit-ups", "Jumping Jacks", "Leg Raises", "Plank"]
    var listOfArmsExercises = ["Dips","Regular Push-ups", "Diamond Push-ups","Wall-Push-ups"]
    var listOfLegsExercises = ["Squats","Jumping Squats", "Backward Lunge", "Leg Lift"]
    var listForPicker: [String] = [String]()
    
    var dataFromSingleExerciseView = ExerciseObject(name: "default", description: "default", guide: "default", difficulty: "default", lenghtAsSec: 0, lenghtAsReps: 0)
    var newExercise = ExerciseObject(name: "default", description: "default", guide: "default", difficulty: "default", lenghtAsSec: 0, lenghtAsReps: 0)
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var difficultyLabel: UILabel!
    @IBOutlet weak var exercisePicker: UIPickerView!
    @IBOutlet weak var recordText: UITextView!
    @IBOutlet weak var clearRecordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        exercisePicker.delegate = self
        exercisePicker.dataSource = self
    
        currentExercise = dataFromSingleExerciseView.name
        nameLabel.text = dataFromSingleExerciseView.name
        difficultyLabel.text = dataFromSingleExerciseView.difficulty
        currentExercise = dataFromSingleExerciseView.description
        
        if(dataFromSingleExerciseView.name == "Core"){
            listForPicker = listOfCoreExercises
            
            var rowIndex = listOfCoreExercises.firstIndex(of: dataFromSingleExerciseView.description)
            if(rowIndex == nil) { rowIndex = 0 }
            exercisePicker.selectRow(rowIndex!, inComponent: 0, animated: true)
        }
        else if(dataFromSingleExerciseView.name == "Arms"){
            listForPicker = listOfArmsExercises
            
            var rowIndex = listOfArmsExercises.firstIndex(of: dataFromSingleExerciseView.description)
            if(rowIndex == nil) { rowIndex = 0 }
            exercisePicker.selectRow(rowIndex!, inComponent: 0, animated: true)
        }
        else if (dataFromSingleExerciseView.name == "Legs"){
            listForPicker = listOfLegsExercises
            
            var rowIndex = listOfLegsExercises.firstIndex(of: dataFromSingleExerciseView.description)
            if(rowIndex == nil) { rowIndex = 0 }
            exercisePicker.selectRow(rowIndex!, inComponent: 0, animated: true)
        }
        
        createNewExercise()
        checkRecords()
        
    }
    
    //Create new exercise to be sent back to previous view
    func createNewExercise(){
        print("Current exercise is: \(currentExercise)")
        let exerciseToSend = ExerciseObject(
            name: dataFromSingleExerciseView.name,
            description: currentExercise,
            guide: Guides().getGuide(exercise: currentExercise),
            difficulty: dataFromSingleExerciseView.difficulty,
            lenghtAsSec: ExerciseData().getLengthAsSecs(difficulty: dataFromSingleExerciseView.difficulty, exercise: currentExercise),
            lenghtAsReps: ExerciseData().getLengthAsReps(difficulty: dataFromSingleExerciseView.difficulty, exercise: currentExercise))
        newExercise = exerciseToSend
    }
    
    //Check if user has previous record set in this exercise
    func checkRecords(){
        print("Checking records")
        var records: [Exercise] = []
        let managedObjectContext = AppDelegate.viewContext
        let exerciseRequest:NSFetchRequest<Exercise> = Exercise.fetchRequest()
        if let exercises = try? managedObjectContext.fetch(exerciseRequest){
            for e in exercises{
                if currentExercise == e.desc && dataFromSingleExerciseView.difficulty == e.difficulty && e.userID == Auth.auth().currentUser?.uid{
                    print("Found previous exercise")
                    records.append(e)
                }
            }
        }
        if(!records.isEmpty){
            var mostReps = records[0]
            for r in records{
                if r.repsDone > mostReps.repsDone{
                    mostReps = r
                }
            }
            print("Your record in this exercise is \(mostReps.repsDone) reps")
            recordText.text = "Your record in this exercise is \(mostReps.repsDone) reps in \(mostReps.timeTaken) seconds."
            recordText.isHidden = false
            clearRecordButton.isHidden = false
            clearRecordButton.isUserInteractionEnabled = true
        }else{
            print("No record saved")
            recordText.isHidden = true
            clearRecordButton.isHidden = true
            clearRecordButton.isUserInteractionEnabled = false
        }
        
    }
    
    @IBAction func clearButtonPressed(_ sender: UIButton) {
        print("Clear button pressed")
        
        let alert = UIAlertController(title: "Are you sure you want to clear all records of this exercise?", message: "This action cannot be undone", preferredStyle: .alert)
        
        let color = #colorLiteral(red: 0.05362936854, green: 0.6396086812, blue: 0, alpha: 1)
        
        alert.view.tintColor = color
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: {action in
            let managedObjectContext = AppDelegate.viewContext
            let exerciseRequest:NSFetchRequest<Exercise> = Exercise.fetchRequest()
            if let exercises = try? managedObjectContext.fetch(exerciseRequest){
                for e in exercises{
                    if self.currentExercise == e.desc && self.dataFromSingleExerciseView.difficulty == e.difficulty{
                        print("Clearing object from Core Data")
                        managedObjectContext.delete(e)
                    }
                }
            }
            self.recordText.text = "Exercise deleted"
            self.clearRecordButton.isHidden = true
        }))
        
        self.present(alert, animated: true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listForPicker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        currentExercise = String(listForPicker[row])
        createNewExercise()
        checkRecords()
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(listForPicker[row])
    }
    
}
