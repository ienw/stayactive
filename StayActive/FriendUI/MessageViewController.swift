//
//  MessageViewController.swift
//  StayActive
//
//  Created by iosdev on 23.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore

class MessageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDataSource, UIPickerViewDelegate, TakeChallengeButtonDelegate {
    
    let db = Firestore.firestore()
    
    // This constraint ties an element at zero points from the bottom layout guide
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var messageTableView: UITableView!
    
    var friendsName = ""
    var friendsId = ""
    
    var currentUserName = ""
    var currentUserId = ""
    var channelId = "testchannel" {
        didSet{
            print("channelId changed to \(channelId)")
        }
    }
    
    // A tuple that holds the information of currently chosen workout challenges
    var challengeToSend: (name: String, difficulty: String) = (name: "", difficulty: "") {
        didSet{
            print("Challenge: \(challengeToSend)")
        }
    }
    
    var messages = [Dictionary<String, Any>](){
        didSet{
            // print("messages dictionary changed: \(messages)" )
        }
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        print("keyboard was shown")
        self.bottomConstraint.constant = 285 // Move view 285 points upward
    }

    @objc func keyboardWillHide(sender: NSNotification) {
         self.bottomConstraint.constant = 20 // Move view to original position
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let currentUser = Auth.auth().currentUser {
            currentUserName = currentUser.displayName ?? "default user"
            print("current user name: \(currentUserName)")
        }
        
        usernameLabel.text = friendsName
        createChannel(currentUserId, friendsId)
        
        messageTableView.rowHeight = UITableView.automaticDimension
        messageTableView.estimatedRowHeight = 80
        
        messageTableView.delegate = self
        messageTableView.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil);

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil);
    }
    
    // For keyboard avoiding functionality
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Channel creation methods
    
    func createChannel(_ firstId: String,_ secondId: String){
        
        let firstSecond = "\(firstId+secondId)"
        // print("Channel id firstSecond: \(firstSecond)")
        
        let secondFirst = "\(secondId+firstId)"
        // print("Channel id secondFirst: \(secondFirst)")
        
        // If channel is already made by current user true is returned. Else it checks if the channel is made by the other user and returns value according to that result
        checkChannel(secondFirst, firstSecond)
    }
    
    /* Channels' names are to be a combination of both users id's, if neither of those combinations already exists on some channel name, there's a new one created with id's in order:
     
     currentUserId + friendsId
     
     This means that when checking if a channel already exists with that name, the query should be:
     
     - friendsId + currentUserId -> We can know if the other user has made the channel already
     - currentUserId + friendsId -> We can know if current user has made the channel already */
    
    // MARK: Channel checkers
    
    func checkChannel(_ id: String, _ id2: String) {
        var existsBoolean = Bool()
        var docRef = self.db.collection("channels").document(id)
        docRef.getDocument { (document, error) in
            if document?.exists ?? false {
                print("Document data: \(String(describing: document!.data()))")
                print("Channel made before with id1: \(id)")
                self.channelId = id
                self.setupChannelListener(self.channelId)
                existsBoolean = true
            } else {
                print("Channel with id: \(id) doesn't exists")
                
                docRef = self.db.collection("channels").document(id2)
                docRef.getDocument { (document, error) in
                    if document?.exists ?? false {
                        print("Document data: \(String(describing: document!.data()))")
                        print("Channel made before with id2: \(id2)")
                        
                        self.channelId = id2
                        self.setupChannelListener(self.channelId)
                        existsBoolean = true
                    } else {
                        print("No channel made before")
                        existsBoolean = false
                        self.createNewChannel(id: id2)
                    }
                }
            }
        }
        print("Exists value: \(existsBoolean)")
    }
    
    // Called if checkchannel can't find any channel with given id's
    func createNewChannel(id: String) {
        print("Channel doesn't exist at all -> creating a new one")
        channelId = id
        
        db.collection("channels").document(channelId).setData(["name":"direct messages","exists": true])
        { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfully written!")
                
                self.setupChannelListener("\(self.channelId)")
            }
        }
    }
    
    // the added snapShotListener updates whenever there's changes to current messages
    func setupChannelListener(_ channelId: String){
        db.collection("channels").document("\(channelId)").collection("messages")
            .addSnapshotListener(includeMetadataChanges: true) { querySnapshot, error in
                guard let documents = querySnapshot?.documents else {
                    print("No documents in collection")
                    return
                }
                
                let messageBody = documents.map { $0["body"]! }
                let messageDate = documents.map { $0["date"]! }
                let messageSender = documents.map { $0["name"]! }
                
                let isMessageChallenge = documents.map { $0["isChallenge"]! }
                
                let messageWorkout = documents.map { $0["workoutName"]! }
                let messageWorkoutDifficulty = documents.map { $0["workoutDifficulty"]! }
                
                let messageBodies = messageBody as? [String] ?? ["Can't convert value"]
                let messageDates = messageDate as? [String] ?? ["Can't convert value"]
                let messageSenders = messageSender as? [String] ?? ["Can't convert value"]
                
                let messageTypes = isMessageChallenge as? [Bool] ?? [false]
                
                let messageWorkouts = messageWorkout as? [String] ?? ["Can't convert value"]
                let messageWorkoutDifficulties = messageWorkoutDifficulty as? [String] ?? ["Can't convert value"]
                
                print("Array of messagetypes: \(messageTypes)")
                
                // self.messages = messageBody as? [String] ?? ["Can't convert value"]
                // print("Current messageArray: \(self.messages)")
                // print("Current messages: \(messageDocuments)")
                
                self.buildMessagesArray(messageSenders, messageDates, messageBodies, messageTypes, messageWorkouts, messageWorkoutDifficulties)
                
                self.updateView()
        }
    }
    
    func buildMessagesArray(_ senders: [String],_ dates: [String],_ bodies: [String],_ booleans: [Bool], _ workouts: [String],_ difficulties: [String]) {
        var tempMessages = [Dictionary<String, Any>()]
        for i in 0..<senders.count {
            let messageObject: Dictionary<String, Any> = ["name": senders[i],
                                                          "date": dates[i],
                                                          "body": bodies[i],
                                                          "isChallenge": booleans[i],
                                                          "workoutName": workouts[i],
                                                          "workoutDifficulty": difficulties[i]]
            
            tempMessages.append(messageObject)
        }
        self.messages = tempMessages
        print("messages dictionary: \(self.messages)")
    }
    
    func updateView() {
        DispatchQueue.main.async
            {
                self.messageTableView.reloadData()
                self.scrollToBottom()
        }
    }
    
    // MARK: User interaction methods
    
    func sendMessage(msgBody: String, isChallenge: Bool, workoutName: String, workoutDifficulty: String){
        let currentDate = getCurrentDateTime()  // Messages are stored using this
        let currentTime = getCurrentTime()      // So messages could show only needed information of time it was sent
        
        var msg = msgBody
        
        if isChallenge == true {
            msg = "Challenge: \(workoutName)"
        }
        db.collection("channels").document(channelId).collection("messages").document("\(currentDate+currentUserName)").setData([
            "isChallenge": isChallenge,
            "workoutName": "\(workoutName)",
            "workoutDifficulty": "\(workoutDifficulty)",
            "name": "\(currentUserName)",
            "body": "\(msg)",
            "date": "\(currentTime)",
            "senderId": "\(currentUserId)"
        ]) { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfully written!")
                
                self.updateView()
            }
        }
    }
    
    // MARK: Message sending functions
    // This is called whenever current user wants to send a message
    @IBAction func sendBtnTapped(_ sender: UIButton) {
        if inputTextField.text != ""{  // So no empty messages could be sent
            let msg = inputTextField.text ?? "input some text"
            sendMessage(msgBody: msg, isChallenge: false, workoutName: "", workoutDifficulty: "")
            inputTextField.text = ""
        }
    }
    
    func sendChallengeToFriend(workoutName: String, workoutDifficulty: String) {
        sendMessage(msgBody: "not a message", isChallenge: true, workoutName: workoutName, workoutDifficulty: workoutDifficulty)
    }
    
    // MARK: Keyboard avoiding function
    func keyboardWasShown(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomConstraint.constant = keyboardFrame.size.height + 20
        })
    }
    
    // MARK: TableView setup
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "MessageCell"
    
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? MessageTableViewCell else {
            fatalError("The cell is not \(cellIdentifier)")
        }
        
        cell.delegate = self
        cell.indexPath = indexPath
        
        let messageText = messages[indexPath.row]["body"]
        let messageSender = messages[indexPath.row]["name"]
        let messageDate = messages[indexPath.row]["date"]
        
        let isChallenge = messages[indexPath.row]["isChallenge"]
        
        let workoutName = messages[indexPath.row]["workoutName"]
        let workoutDifficulty = messages[indexPath.row]["workoutDifficulty"]
        
        cell.messageBodyLabel.text = messageText as? String ?? ""
        cell.userNameLabel.text = messageSender as? String ?? ""
        cell.dateLabel.text = messageDate as? String ?? ""
        
        if isChallenge as? Bool ?? false{
            cell.takeChallengebutton.isHidden = false
            cell.workout.name = workoutName as? String ?? ""
            cell.workout.difficulty = workoutDifficulty as? String ?? ""
        } else {
            cell.takeChallengebutton.isHidden = true
        }
        
        return cell
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.messages.count-1, section: 0)
            self.messageTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    // MARK: Date / time functions
    
    // Return the whole date
    func getCurrentDateTime () -> String {
        let dateFormatter : DateFormatter = DateFormatter()
        //  dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.dateFormat = "HH:mm:ss dd-MMM yyyy"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        
        return dateString
    }
    
    // Return only current hour and min
    func getCurrentTime () -> String {
        let dateFormatter : DateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "HH.mm"
        let date = Date()
        let timeString = dateFormatter.string(from: date)
        
        return timeString
    }
    

    
    // MARK: PickerView setup
    
    // This pickerView is used on the alertView that let's the user choose a challenge to send to other user
    
    let difficultyArray = ["Beginner", "Regular", "Athlete"]
    
    let workoutArray = ["Sit-ups", "Jumping Jacks", "Leg Raises", "Plank", "Dips", "Regular Push-ups", "Diamond Push-ups", "Wall-Push-ups", "Squats", "Jumping Squats", "Backward Lunge", "Leg Lift"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return workoutArray.count
        }else if component == 1 {
            return difficultyArray.count
        }else {
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return "\(workoutArray[row])"
        }
        else if component == 1 {
            return "\(difficultyArray[row])"
        } else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let workoutIndex = pickerView.selectedRow(inComponent: 0)
        let difficultyIndex = pickerView.selectedRow(inComponent: 1)
        challengeToSend.name = "\(workoutArray[workoutIndex])"
        challengeToSend.difficulty = "\(difficultyArray[difficultyIndex])"
    }
    
    @IBAction func challengeTapped(_ sender: UIButton) {
        challengeFriend()
    }
    
    func challengeFriend() {
        let alertView = UIAlertController(
            title: "Select item from list",
            message: "\n\n\n\n\n\n\n\n\n",
            preferredStyle: .alert)
        
        
        let pickerView = UIPickerView(frame:
            CGRect(x: 0, y: 50, width: 260, height: 162))
        
        pickerView.dataSource = self
        pickerView.delegate = self
        
        // comment this line to use white color
        pickerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        
        alertView.view.addSubview(pickerView)
        
        /*
         let action = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
         
         alertView.addAction(action)
         */
        
        let okAction = UIAlertAction(title: "Send", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            
            self.sendChallengeToFriend(workoutName: self.challengeToSend.name, workoutDifficulty: self.challengeToSend.difficulty)
            // Here the function for sending an exerciseMessage to other user
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            
            // This doesn't need to do anything
        }
        
        alertView.addAction(okAction)
        alertView.addAction(cancelAction)
        
        present(alertView, animated: true, completion: { pickerView.frame.size.width = alertView.view.frame.size.width } )
    }
    
    // MARK: Setup of current user segue
    
    func prepareExercise(workoutName: String, workoutDescription: String, workoutDifficulty: String) -> ExerciseObject {
        
        return ExerciseObject(name: workoutName, description: workoutDescription, guide: Guides().getGuide(exercise: workoutDescription), difficulty: workoutDifficulty, lenghtAsSec: ExerciseData().getLengthAsSecs(difficulty: workoutDifficulty, exercise: workoutDescription), lenghtAsReps: ExerciseData().getLengthAsReps(difficulty: workoutDifficulty, exercise: workoutDescription))
    }
    
    func takeChallengeTapped(at index: IndexPath, workoutName: String, workoutDifficulty: String) {
        print("Challenge \(workoutName) taken at index \(index.row)")
            
        let workoutDescription = workoutName

        // Because I stupidly didn't include actual workout categories, I'll just put the name in this function
        func decideName(description: String) -> String{
            
            if description == "Sit-ups" {
                return "Core"
            }
            
            if description == "Plank" {
                return "Core"
            }
            
            if description == "Squats" {
                return "Legs"
            }
            
            if description == "Jumping Squats" {
                return "Legs"
            }
            
            if description == "Backward Lunge" {
                return "Legs"
            }
            
            if description == "Jumping Jacks" {
                return "Legs"
            }
            
            if description == "Leg Raises" {
                return "Legs"
            }
            
            if description == "Running" {
                return "Cardio"
            }
        
            if description == "Dips" {
                return "Arms"
            }
            
            if description == "Regular Push-ups" {
                return "Arms"
            }
            
            if description == "Diamond Push-Ups" {
                return "Arms"
            }
            
            if description == "Wall-Push-Ups" {
                return "Arms"
            }
            
            else {
                return "Core"
            }
            
        }
        
        let workoutCategory = decideName(description: workoutDescription)
        
        let exerciseForVc = prepareExercise(workoutName: workoutCategory, workoutDescription: workoutName, workoutDifficulty: workoutDifficulty)
        
        let storyboard = UIStoryboard(name: "ExerciseStoryboard", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SingleExerciseViewController") as! SingleExerciseViewController
        
        vc.exercise = exerciseForVc
        vc.backButton?.isHidden = true
        
        navigationController?.pushViewController(vc, animated: true)
        
    }
}
