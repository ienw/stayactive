//
//  Structs.swift
//  StayActive
//
//  Created by iosdev on 28.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import Foundation

struct Friend {
    var name: String
    var uid: String
    var email: String
    var profilePic: URL?
    
    init(name: String, uid: String, email: String, profilePic: URL? = nil){
        self.name = name
        self.uid = uid
        self.email = email
        self.profilePic = profilePic
    }
}
