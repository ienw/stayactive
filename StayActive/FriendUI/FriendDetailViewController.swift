//
//  FriendDetailViewController.swift
//  StayActive
//
//  Created by iosdev on 23.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class FriendDetailViewController: UIViewController {

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    
    let db = Firestore.firestore()
    
    // FriendsName is only the name of the selected user
    var friendsName = ""
    
    // FriendsNameWithId is the full document id from firestore. It's user's name with the first 4 letters of their id
    var friendsNameWithId = ""

    // FriendsId is supposed to be the selected user's ID
    var friendsId = ""
    
    // CurrentUserId is the id of current user logged in to the app
    var currentUserId = ""
    
    // userInfo[0] = id ,[1] = email, [2]  = image
    var friendsInfo = (uid: "uid", email: "email",image: URL(string: "https://placekitten.com/220/220"))
    
    override func viewDidLoad() {
        super.viewDidLoad()

        usernameLabel.text = friendsName
        emailLabel.text = friendsInfo.email
        
        // This to be added later, need to make a custom UIImageview from code to show image from URL
        // userImageView.image = userInfo.image
        
        print("User's \(friendsName) id: \(friendsNameWithId)")
        
        loadUserData(friendsNameWithId)
        
        print("Friends ID in details: \(friendsInfo.uid)")
        
        // Do any additional setup after loading the view.
    }
    @IBAction func messageButtonTapped(_ sender: UIButton) {
        
        // Take the user to messaging view where they can just talk about anything and send challenges to eachother
        
        print("Sending message to user \(usernameLabel.text ?? "no text")")
    }
    
    @IBAction func challengeButtonTapped(_ sender: UIButton) {
        
        // Take the user to the challenge selection view where they can send a workout challenge they want
        print("Challengin user \(usernameLabel.text ?? "no text")")
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.destination is MessageViewController{
            let vc = segue.destination as? MessageViewController
            
            // Data is passed to messageView
            vc?.friendsName = self.friendsName
            
            // FriendsId is selected users Id
            vc?.friendsId = self.friendsInfo.uid
            vc?.currentUserId = self.currentUserId
        }
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    func loadUserData(_ id: String) {
        let docReference = db.collection("users").document(id)
        
        docReference.getDocument { (document, error) in
            if let document = document, document.exists {
                let dataDescription = document.data()
                print("Document data: \(String(describing: dataDescription))")
                print("Type of data: \(type(of: dataDescription))")
                
                // This part parses the data from firestore into different constants
                let userId = dataDescription!["uid"] as? String ?? "no id"
                let userEmail = dataDescription!["email"] as? String ?? "no email"
                let userImage = dataDescription!["image"] as? URL ?? URL(string: "https://placekitten.com/200/200")
                
                // This part puts the data into the userInfo tuple
                self.friendsInfo = (uid: userId, email: userEmail, image: userImage)
                print("userInfo: \(self.friendsInfo)")
                
                // Update the view after fetching
                DispatchQueue.main.async{
                    self.updateView()
                }
                
            }else {
                // This should only happen if back-end doesn't exist as designed
                print("Document does not exist")
                
            }
        }
    }
    
    func updateView(){
        emailLabel.text = friendsInfo.email
    }
    
    // This is not used atm, kept for possible later use
    /*
    func parseUserDataJson (_ dataString: String){
        let data = dataString.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]{
                print("json array: \(jsonArray)")
            }else {
                print("bad json")
            }
        }catch{
            print("Parse error: \(error)")
        }
        
    }
 
 */
}
