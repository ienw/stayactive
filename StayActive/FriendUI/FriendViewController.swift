//
//  FriendViewController.swift
//  StayActive
//
//  Created by iosdev on 21.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class FriendViewController: UIViewController {

    @IBOutlet weak var friendListTableView: UITableView!
    
    let db = Firestore.firestore()
    
    var currentUserId = ""
    
    // userIds array is to store collection's ids' as is so we could get the information later on the friendDetailView, users is an array of usernames
    var users: [String] = []
    var userNamesWithIds: [String] = []
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        friendListTableView.dataSource = self
        friendListTableView.delegate = self
        
        // friendListTableView.register(FriendsTableViewCell.self, forCellReuseIdentifier: "FriendCell")
        // Do any additional setup after loading the view.

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FriendViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "FriendCell"
        let friendName = users[indexPath.row]
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? FriendsTableViewCell else {
            fatalError("The cell is not FriendsTableViewCell")
        }
        
        cell.nameLabel?.text = friendName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = storyboard?.instantiateViewController(identifier: "FriendDetailViewController") as? FriendDetailViewController else {
            fatalError("Cannot instantiateViewController")
        }
        print("viewController instantiated")
        
        print("indexpath.row: \(users[indexPath.row])")
        
        // These pass the needed data to the viewController that shows chosen user's info, currentUserId is needed later to create chatChannels
        let friendsId = userNamesWithIds[indexPath.row]
        let friendsName = users[indexPath.row]
        
        self.navigationController?.pushViewController(vc, animated: true)
        vc.friendsNameWithId = friendsId
        vc.friendsName = friendsName
        vc.currentUserId = currentUserId
    }

    func loadData() {
        var channelReference: CollectionReference {
            return db.collection("users")
        }
        
        db.collection("users").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("error getting documents: \(err)")
            } else {
                self.users.removeAll()
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    let withoutId = document.documentID.dropLast(5)
                    
                    self.users.append(String(withoutId))

                    self.userNamesWithIds.append(document.documentID)
                }
                DispatchQueue.main.async
                {
                    self.friendListTableView.reloadData()
                }
            }
        }
        
        // This loads the userID of current user, need it for later to create the chat channel
        if let user = Auth.auth().currentUser  {
            let uid: String = user.uid
            currentUserId = uid
        }
    }
    
    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is ChallengeViewController
        {
            if let indexPath = sender as? IndexPath {
                let friendName = friendList[indexPath.row]
                let vc = segue.destination as? ChallengeViewController
                vc?.friendNameLabel.text = "Challenge \(friendName)"
            }
        }
    }
    */
}
