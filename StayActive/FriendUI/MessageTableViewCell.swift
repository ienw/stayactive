//
//  MessageTableViewCell.swift
//  StayActive
//
//  Created by iosdev on 3.5.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

protocol TakeChallengeButtonDelegate{
    func takeChallengeTapped(at index:IndexPath, workoutName: String, workoutDifficulty: String)
}

class MessageTableViewCell: UITableViewCell {

    var delegate:TakeChallengeButtonDelegate!
    var indexPath:IndexPath!
    
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var messageBodyLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var takeChallengebutton: UIButton!
    
    var workout: (name: String, difficulty: String) = (name: "", difficulty: ""){
        didSet{
            // print("Workout set for cell: \(workout)")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        // super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func takeChallengeTapped(_ sender: UIButton) {
        print("take challenge with info: \(workout) tapped")
        self.delegate?.takeChallengeTapped(at: indexPath, workoutName: workout.name, workoutDifficulty: workout.difficulty)
    }
    
}
