//
//  FriendsTableViewCell.swift
//  StayActive
//
//  Created by iosdev on 22.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

class FriendsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func challengeTapped(_ sender: UIButton) {
        
        print("Challenge pressed for friend \(nameLabel.text ?? "couldn't get name")")
    }
    

}
