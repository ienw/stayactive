//
//  Guides.swift
//  StayActive
//
//  Created by iosdev on 28.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//
//  Simple class that contains guides to different exercises

import Foundation

public class Guides{
    
    var guideList: [String:String] = [
        "Cardio": "Just start running. Simple as that. You want to get your heart rate up and sweat going, but don't over do it.  Keep your pace such that you should be able to talk all the time. Remember too keep safe distance to other people you might run into when you are outside your home.",
        "Sit-ups": "Lay down on the ground on your back and try to get some weight on your feet. Maybe push them under your sofa or ask a family member to stand on them. Then just simply rise up to sitting position. Keep your hands behind your back or crossed on your chest. Do not reach out with your arms.",
        "Dips": "You will need sturdy but low table or hard bench. Simply sit on it with your arms on the sides and push your ass towards so you are sitting in the air, with your arms holding you up on the bench. Then try to get low as possible with raising yourself up afterwards with your arms. Try not to help yourself with your legs, keep them straight at all times",
        "Squats": "Take a shoulder wide stand with your feet in 45 degree angle. Simply sit down while trying to keep your back straight as possible. It might help to keep your hands on your waist or behind your back. Rise back up and repeat.",
        "Jumping Jacks": "Stand upright with your legs together, arms at your sides.Bend your knees slightly, and jump into the air. As you jump, spread your legs to be about shoulder-width apart. Stretch your arms out and over your head. Jump back to starting position. Repeat.",
        "Leg Raises": "Lie on your back, legs straight and together. Keep your legs straight and lift them all the way up to the ceiling until your butt comes off the floor. Slowly lower your legs back down till they’re just above the floor. Hold for a moment. Raise your legs back up. Repeat.",
        "Plank": "Plant hands directly under shoulders (slightly wider than shoulder width) like you’re about to do a push-up. Ground toes into the floor and squeeze glutes to stabilize your body. Your legs should be working, too — be careful not to lock or hyperextend your knees. Neutralize your neck and spine by looking at a spot on the floor about a foot beyond your hands. Your head should be in line with your back. Hold the position for 20 seconds. As you get more comfortable with the move, hold your plank for as long as possible without compromising your form or breath.",
        "Regular Push-ups": "Get down on all fours, placing your hands slightly wider than your shoulders. Straighten your arms and legs. Lower your body until your chest nearly touches the floor. Pause, then push yourself back up. Repeat.",
        "Diamond Push-ups": "Get down on all fours, placing your hands so that your thumbs and index fingers create a \"diamond\". Straighten your arms and legs. Lower your body until your chest nearly touches the floor. Pause, then push yourself back up. Repeat.",
        "Wall-Push-ups": "Stand in front of a wall one big step away from it. Then put your hands out straight towards the wall and lean against it. Lift your heels. Slowly bend your elbows and press your upper body towards the wall. Push back and repeat the exercise. Remember to keep your body straight",
        "Jumping Squats": " Stand with your feet shoulder-width apart. Start by doing a regular squat, then engage your core and jump up explosively. When you land, lower your body back into the squat position to complete one rep. Land as quietly as possible, which requires control. Repeat.",
        "Backward Lunge": "Stand upright, with your hands at your hips. Take a large step backward with your left foot. Lower your hips so that your right thigh (front leg) becomes parallel to the floor with your right knee positioned directly over your ankle. Your left knee should be bent at a 90-degree angle and pointing toward the floor with your left heel lifted. Return to standing by pressing your right heel into the floor and bringing your left leg forward to complete one rep. Alternate legs, and step back with right leg.",
        "Leg Lift": "Lie down on your right side on a mat or the floor. Your body should be in a straight line with your legs extended and feet stacked on top of each other. Place your arm straight on the floor under your head or bend your elbow and cradle your head for support. Place your left hand out front for extra support or let it rest on your leg or hip. As you exhale, gently raise your left leg off the lower leg. Stop raising your leg when you feel the muscles flex in your lower back or obliques. Inhale and lower the leg back down to meet the right leg. Stack your feet again. Change sides after the set and repeat"
    ]
    
    func getGuide(exercise : String) -> String{
        return guideList[exercise]!
    }
}



