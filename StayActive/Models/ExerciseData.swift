//
//  ExerciseData.swift
//  StayActive
//
//  Created by iosdev on 30.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import Foundation

public class ExerciseData{
    
    func getLengthAsSecs(difficulty:String, exercise:String) -> Int{
        if(exercise != "Running"){
            if(difficulty == "Beginner"){
                if(exercise == "Jumping Jacks" || exercise == "Plank"){
                    return 20
                }else{
                    return 30
                }
            }else if(difficulty == "Regular"){
                if(exercise == "Jumping Jacks"){
                    return 30
                }else if(exercise == "Plank"){
                    return 40
                }else{
                    return 45
                }
            }else{
                if(exercise == "Jumping Jacks"){
                    return 40
                }else{
                    return 60
                }
            }
        }else{
            if(difficulty == "Beginner"){
                return 300
            }else if(difficulty == "Regular"){
                return 600
            }else{
                return 900
            }
        }
    }
    func getLengthAsReps(difficulty:String, exercise:String) -> Int{
        if(exercise != "Running"){
            if(difficulty == "Beginner"){
                if(exercise == "Sit-ups"){
                    return 13
                }else if(exercise == "Jumping Jacks"){
                    return 18
                }else if(exercise == "Leg Raises"){
                    return 16
                }else if(exercise == "Plank"){
                    return 1
                }else if(exercise == "Dips"){
                    return 10
                }else if(exercise == "Regular Push-ups"){
                    return 10
                }else if(exercise == "Diamond Push-ups"){
                    return 5
                }else if(exercise == "Diagonal Plank"){
                    return 10
                }else if(exercise == "Squats"){
                    return 12
                }else if(exercise == "Jumping Squats"){
                    return 5
                }else if(exercise == "Backward Lunge"){
                    return 14
                }else{
                    return 12
                }
                
            }else if(difficulty == "Regular"){
                if(exercise == "Sit-ups"){
                    return 20
                }else if(exercise == "Jumping Jacks"){
                    return 25
                }else if(exercise == "Leg Raises"){
                    return 18
                }else if(exercise == "Plank"){
                    return 1
                }else if(exercise == "Dips"){
                    return 14
                }else if(exercise == "Regular Push-ups"){
                    return 15
                }else if(exercise == "Diamond Push-ups"){
                    return 8
                }else if(exercise == "Diagonal Plank"){
                    return 15
                }else if(exercise == "Squats"){
                    return 18
                }else if(exercise == "Jumping Squats"){
                    return 9
                }else if(exercise == "Backward Lunge"){
                    return 18
                }else{
                    return 18
                }
            }else{
                if(exercise == "Sit-ups"){
                    return 24
                }else if(exercise == "Jumping Jacks"){
                    return 28
                }else if(exercise == "Leg Raises"){
                    return 22
                }else if(exercise == "Plank"){
                    return 1
                }else if(exercise == "Dips"){
                    return 22
                }else if(exercise == "Regular Push-ups"){
                    return 22
                }else if(exercise == "Diamond Push-ups"){
                    return 13
                }else if(exercise == "Diagonal Plank"){
                    return 19
                }else if(exercise == "Squats"){
                    return 24
                }else if(exercise == "Jumping Squats"){
                    return 15
                }else if(exercise == "Backward Lunge"){
                    return 24
                }else{
                    return 23
                }
            }
        }else{
            return 0
        }
    }
}
