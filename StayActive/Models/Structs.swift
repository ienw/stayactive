//
//  Structs.swift
//  StayActive
//
//  Created by iosdev on 27.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import Foundation

struct ExerciseObject{
    let name: String
    let description: String
    let guide: String
    let image: String?          // No idea what kind of value here but it's optional anyway
    
    var dateDone: Date?         // This should only be initialized for a saved exercise
    let difficulty: String      // This could also be an object that returns 1 of 3 values possible for difficulty
    let lenghtAsSec: Int     // How much the exercise is planned to go on
    var timeTaken: Int       // How much the exercise took time doing
    var repsDone: Int       // How many reps were done
    
    let lenghtAsReps: Int       // How many repeated moves are recommended, depends on difficulty
    
    // Needs methods to change values and for example set dateDone for saving the exercise
    init(name: String, description: String, guide: String, image: String? = nil, dateDone: Date? = nil, difficulty: String,  lenghtAsSec: Int, timeTaken: Int? = nil, lenghtAsReps: Int, repsDone: Int? = nil){
        self.name = name
        self.description = description
        self.guide = guide
        self.image = image
        
        self.dateDone = dateDone
        self.timeTaken = timeTaken ?? 0
        
        self.difficulty = difficulty
        self.lenghtAsSec = lenghtAsSec
        self.lenghtAsReps = lenghtAsReps
        self.repsDone = repsDone ?? 0
        
    }
}


struct UserObject{
    let name: String
    let uid: String?      // idk if it's okay to store uid locally like this. Supposed to be the firebase user uid
    let gender: String?
    
    let height: Int?
    let weight: Int?
    
    // The User can be initialized with only name and uid for now, other values can be Nil
    
    // User still needs methods to change and add variables etc
    init(name: String, uid: String? = nil, gender: String? = nil, height: Int? = nil, weight: Int? = nil){
        self.name = name
        self.uid = uid
        self.gender = gender
        self.height = height
        self.weight = weight
    }
}

