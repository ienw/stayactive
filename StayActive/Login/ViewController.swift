//
//  Created by iosdev on 14.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit
import Firebase
import FirebaseUI
import FirebaseAuth
import FirebaseFirestore

class ViewController: UIViewController {

    @IBOutlet weak var textLabel: UILabel!
    
    let db = Firestore.firestore()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func loginPressed(_ sender: UIButton) {
        
        
        // Get the deafault auth UI obj
        guard let authUI = FUIAuth.defaultAuthUI() else {
            print("Couldn't set authUI")
            return
        }
        
        // Set the delegate
        authUI.delegate = self
        authUI.providers = [FUIEmailAuth()]
        
        // Get a reference to the auth UI view controller
        let authViewController = authUI.authViewController()
        
        // Show it
        present(authViewController, animated: true, completion: nil)
    }
}

extension ViewController: FUIAuthDelegate{
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        
        // Check if there wasn't error
        if error != nil {
            //Happens in case if there is an error
            print("Error in authUI: ", error ?? "no error")
            return
        }
        
        // With this call we can get users uid
        print("Dataresult auth: \(authDataResult?.user.uid ?? "didn't get value")")
        
        if let user = Auth.auth().currentUser  {
            var providerID: String = user.providerID
            var email: String? = user.email
            var uid: String = user.uid
            var displayName: String? = user.displayName
            var photoURL: URL? = user.photoURL
            
 
            // Well take the first 4 letters of uid to add to the document name to make duplicate documents less possible in db
            let shortUid = String(uid.prefix(4))
            print("shortUid: \(shortUid)")
            
            var channelReference: CollectionReference {
              return db.collection("users")
            }

            // Add a new user document in collection "users"
            // User document is saved with user name + first 4 letters of uid so there couldn't be 2 of the same documents
            // User document has the user id and user name with it. We'll use this collection later to show all registered users in the friendsTableView
            channelReference.document("\(displayName ?? "no user name")#\(shortUid)").setData([
                "image": photoURL ?? "https://placekitten.com/200/200",
                "email": email ?? "no email",
                "uid": uid
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
        }

        
        performSegue(withIdentifier: "goToMain", sender: self)
    }
}
