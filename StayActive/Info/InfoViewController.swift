//
//  InfoViewController.swift
//  StayActive
//
//  Created by iosdev on 21.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.text = "This is an application with the purpose of encouraging it's users to keep staying physically active even when they're not able to go to the gym during quarantine. With Stay Active, users can look up how to do simple workouts from home, set goals and keep track of done workouts. The user can also challenge other users to motivate them to do workouts too. \n\nStay Active was developed by: \n\nMatias Jalava \n\nToni Soini \n\nI-En Wu \n\nLauri Nissinen"
        
    }
}
