//
//  EditProfileViewController.swift
//  StayActive
//
//  Created by iosdev on 23.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit
import CoreData

class EditProfileViewController: UIViewController, UITextFieldDelegate {

    // MARK: Properties
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var ageField: UITextField!
    var age: Int?
    @IBOutlet weak var genderField: UITextField!
    var gender: String?
    @IBOutlet weak var heightField: UITextField!
    var height: Double?
    @IBOutlet weak var weightField: UITextField!
    var weight: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        setData()
        
        popupView.layer.cornerRadius = 10
        popupView.layer.masksToBounds = true

        // Do any additional setup after loading the view.
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("textField: Allow editing")
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        // Disable the Save button while editing
        saveButton.isEnabled = false
        print("textField: User is editing")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateSaveButtonState()
        print("textField: Editing is done")
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard
        textField.resignFirstResponder()
        print("textfield: Enter was pressed")
        return true
    }
    
    // MARK: Navigation

    @IBAction func cancelPopup(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func savePopup(_ sender: UIButton) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "User", in: context)
        let newEntity = NSManagedObject(entity: entity!, insertInto: context)
        
        // Insert new user info here
        newEntity.setValue(Int(ageField.text!), forKey: "age")
        newEntity.setValue(genderField.text, forKey: "gender")
        newEntity.setValue(Double(heightField.text!), forKey: "height")
        newEntity.setValue(Double(weightField.text!), forKey: "weight")
        
        do {
            try context.save()
            print("Edit Profile: Saved successfully")
        } catch {
            print("Edit Profile: Failed miserably")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Core Data
    
    func setData() {
        ageField.text = "\(age ?? 0)"
        genderField.text = "\(gender ?? "Unknown")"
        heightField.text = "\(height ?? 0.0)"
        weightField.text = "\(weight ?? 0.0)"
        
        // Connect Data
        ageField.delegate = self
        genderField.delegate = self
        heightField.delegate = self
        weightField.delegate = self
    }
    
    func getData() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                age = data.value(forKey: "age") as? Int
                gender = data.value(forKey: "gender") as? String
                height = data.value(forKey: "height") as? Double
                weight = data.value(forKey: "weight") as? Double
            }
            print("getData(): Success")
        } catch {
            print("getData(): Failed")
        }
    }
    
    // MARK: Private Methods
    
    private func updateSaveButtonState() {
        // Disable the Save button if the text field is empty
        let text1 = ageField.text ?? ""
        let text2 = genderField.text ?? ""
        let text3 = heightField.text ?? ""
        let text4 = weightField.text ?? ""
        if !text1.isEmpty && !text2.isEmpty && !text3.isEmpty && !text4.isEmpty {
            saveButton.isEnabled = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacter1 = CharacterSet.letters
        let allowedCharacter2 = CharacterSet.decimalDigits
        let allowedCharacter3 = CharacterSet.decimalDigits.union (CharacterSet (charactersIn: "."))
        let characterSet = CharacterSet(charactersIn: string)
        
        let currentText = textField.text ?? ""
        
        guard let stringRange = Range(range, in: currentText) else { return false}
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        switch textField {
        case ageField:
            return allowedCharacter2.isSuperset(of: characterSet) && updatedText.count <= 3
        case genderField:
            return allowedCharacter1.isSuperset(of: characterSet) && updatedText.count <= 12
        case heightField:
            let dotCount = (textField.text?.components(separatedBy: ".").count)! - 1
            
            if dotCount > 0 && string == "." {
                return false
            }
            return allowedCharacter3.isSuperset(of: characterSet) && updatedText.count <= 5
        case weightField:
            let dotCount = (textField.text?.components(separatedBy: ".").count)! - 1
            
            if dotCount > 0 && string == "." {
                return false
            }
            return allowedCharacter3.isSuperset(of: characterSet) && updatedText.count <= 5
        default:
            return allowedCharacter1.isSuperset(of: characterSet) || allowedCharacter3.isSuperset(of: characterSet)
        }
    }
}
