//
//  ProfileViewController.swift
//  StayActive
//
//  Created by iosdev on 21.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseUI
import FirebaseAuth
import FirebaseStorage

class ProfileViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    // MARK: Properties
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    var profileImg = UIImage()
    var picker = UIImagePickerController()
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    var age: Int?
    @IBOutlet weak var genderLabel: UILabel!
    var gender: String?
    @IBOutlet weak var heightLabel: UILabel!
    var height: Double?
    @IBOutlet weak var weightLabel: UILabel!
    var weight: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        setData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getData()
        setData()

    }
    
    // MARK: UIImagePickerControllerDelegate
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // Dismiss the picker if the user canceled
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.modalPresentationStyle = .overFullScreen
        
        // The info dictionary may contain multiple representations of the image. You want to use the original
        guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        // Set photoImageView to display the selected image
        photoImageView.image = selectedImage
        
        // Save image
        saveImage(image: selectedImage)
        
        // Dismiss the picker
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Action
    
    @IBAction func changeAvatar(_ sender: UIButton) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func logoutButton(_ sender: UIButton) {
        try! Auth.auth().signOut()
        
        /* Storyboard change with code
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "firstViewController")
        self.present(vc, animated: false, completion: nil)
         */
    }
    
    // MARK: Private Methods
    
    func setData() {
        ageLabel.text = "\(age ?? 0)"
        genderLabel.text = "\(gender ?? "Unknown")"
        heightLabel.text = "\(height ?? 0.0)"
        weightLabel.text = "\(weight ?? 0.0)"
        photoImageView.image = UIImage(named: "defaultUser")
        photoImageView.image = profileImg
    }
    
    func getData() {
        
        // Get name & email from Firebase
        let user = Auth.auth().currentUser
        let displayName: String? = user?.displayName
        let email: String? = user?.email
        nameLabel.text = displayName
        emailLabel.text = email
        
        // Get Core Data stuff
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                age = data.value(forKey: "age") as? Int
                gender = data.value(forKey: "gender") as? String
                height = data.value(forKey: "height") as? Double
                weight = data.value(forKey: "weight") as? Double
                let imageData = data.value(forKey: "profileImg")
                if imageData != nil {
                    profileImg = UIImage(data: imageData as! Data)!
                }
            }
        } catch {
            print("Failed: getData")
        }
    }
    
    func saveImage(image: UIImage) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "User", in: context)
        let newEntity = NSManagedObject(entity: entity!, insertInto: context)
        
        let imageData: NSData = image.pngData()! as NSData
        // Insert new user info here
        newEntity.setValue((imageData), forKey: "profileImg")
        
        do {
            try context.save()
            print("Edit Profile: Core Data saved successfully")
        } catch {
            print("Edit Profile: Failed saving Core Data")
        }
    }
}
